package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;

import eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakRoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.SearchRolesFilterParameterDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.usersroles.mappers.RoleMapperV1Impl;
import eu.europa.ec.simpl.usersroles.utils.DtoUtils;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    RoleControllerV1.class,
    RoleMapperV1Impl.class,
})
public class RoleControllerV1Test {

    private static UUID randomUUID = UUID.fromString("914c1082-b495-418a-8064-028b6728cf17");

    @MockitoBean
    private RoleController controller;

    @Autowired
    private RoleControllerV1 controllerV1;

    @Test
    void createTest() {
        var requestBody = a(KeycloakRoleDTO.class);
        var responseC0 = a(RoleDTO.class);
        given(controller.create(argThat(dto0 -> DtoUtils.jsonCompare(dto0, requestBody))))
                .willReturn(responseC0);
        var result = controllerV1.create(requestBody);
        assertThat(result.getName()).isEqualTo(responseC0.getName());
        assertThat(DtoUtils.jsonCompare(result, responseC0)).isTrue();
    }

    @Test
    void deleteAttributeFromRoleTest() {
        var requestRoleId = randomUUID;
        var attributeCode = "junit-attribute-code";
        controllerV1.deleteAttributeFromRole(requestRoleId, attributeCode);
        verify(controller).deleteAttributeFromRole(eq(requestRoleId), eq(attributeCode));
    }

    @Test
    void deleteAttributeFromRoleDeprecatedTest() {
        var requestRoleId = randomUUID;
        var attributeCode = "junit-attribute-code";
        controllerV1.deleteAttributeFromRoleDeprecated(requestRoleId, attributeCode);
        verify(controller).deleteAttributeFromRole(eq(requestRoleId), eq(attributeCode));
    }

    @Test
    void deleteCredentialTest() {
        var requestRoleId = randomUUID;
        controllerV1.deleteCredential(requestRoleId);
        verify(controller).delete(eq(requestRoleId));
    }

    @Test
    void duplicateIdentityAttributeToAnOtherRoleTest() {
        var requestId = randomUUID;
        var requestBody = "junit-body";
        controllerV1.duplicateIdentityAttributeToAnOtherRole(requestId, requestBody);
        verify(controller).duplicateIdentityAttributeToAnOtherRole(eq(requestId), eq(requestBody));
    }

    @Test
    void duplicateIdentityAttributeToAnOtherRoleDeprecatedTest() {
        var requestId = randomUUID;
        var requestBody = "junit-body";
        controllerV1.duplicateIdentityAttributeToAnOtherRoleDeprecated(requestId, requestBody);
        verify(controller).duplicateIdentityAttributeToAnOtherRole(eq(requestId), eq(requestBody));
    }

    @Test
    void findByIdTest() {
        var roleId = randomUUID;
        var responseC0 = a(RoleDTO.class);
        given(controller.findById(roleId)).willReturn(responseC0);
        var result = controllerV1.findById(roleId);
        verify(controller).findById(eq(roleId));
        assertThat(DtoUtils.jsonCompare(responseC0, result)).isTrue();
    }

    @Test
    void getIdentityAttributesFromRoleListTest() {
        var requestBody = List.of("junti-input-body1", "junti-input-body2");
        var responseBodyC0 = List.of("junti-output-body1", "junti-output-body2");
        given(controller.getIdentityAttributesFromRoleList(argThat(l -> l.containsAll(requestBody))))
                .willReturn(responseBodyC0);
        var result = controllerV1.getIdentityAttributesFromRoleList(requestBody);
        verify(controller).getIdentityAttributesFromRoleList(eq(requestBody));
        assertThat(result).containsAll(responseBodyC0);
    }

    @Test
    void importRolesTest() {
        var requestBody = List.of(a(KeycloakRoleDTO.class));
        controllerV1.importRoles(requestBody);
        verify(controller).importRoles(argThat(l -> DtoUtils.jsonCompare(l.getFirst(), requestBody.getFirst())));
    }

    @Test
    void replaceIdentityAttributesTest() {
        var requestId = randomUUID;
        var requestBody = List.of("junit-body");
        controllerV1.replaceIdentityAttributes(requestId, requestBody);
        verify(controller).replaceIdentityAttributes(eq(requestId), eq(requestBody));
    }

    @Test
    void searchRolesTest() {
        Integer page = 1;
        Integer size = 10;
        List<String> sort = Collections.emptyList();
        SearchRolesFilterParameterDTO filter = a(SearchRolesFilterParameterDTO.class);
        controllerV1.searchRoles(page, size, sort, filter);
        verify(controller)
                .search(
                        argThat(filterC0 -> DtoUtils.jsonCompare(filterC0, filter)),
                        argThat(pageable -> pageable.getPageSize() == size && pageable.getPageNumber() == page));
    }

    @Test
    void updateTest() {
        var roleId = randomUUID;
        var roleDto = a(eu.europa.ec.simpl.api.usersroles.v1.model.RoleDTO.class);
        RoleDTO dtoResponseC0 = a(RoleDTO.class);
        given(controller.update(argThat(dto -> DtoUtils.jsonCompare(roleDto, dto))))
                .willReturn(dtoResponseC0);
        var result = controllerV1.update(roleId, roleDto);
        assertThat(DtoUtils.jsonCompare(result, dtoResponseC0)).isTrue();
    }
}
