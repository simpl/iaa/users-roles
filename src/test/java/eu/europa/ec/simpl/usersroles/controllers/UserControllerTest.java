package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static eu.europa.ec.simpl.usersroles.utils.TestsUtils.jsonSerialize;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.BDDAssertions.catchException;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import eu.europa.ec.simpl.common.filters.KeycloakUserFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.usersroles.exceptions.EmptyRolesException;
import eu.europa.ec.simpl.usersroles.services.IdentityAttributeService;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        properties = {
            "keycloak.app.realm=authority",
            "microservices.identityProvider.url=http://identity-provider.local"
        })
class UserControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private UserController userController;

    @Mock
    private KeycloakUserService keycloakUserService;

    @Mock
    private IdentityAttributeService identityAttributeService;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    void getUserTest() throws Exception {
        var email = "tolkien@lotr.com";
        var expected = a(KeycloakUserDTO.class);

        when(keycloakUserService.getUserByEmail(email)).thenReturn(expected);

        mockMvc.perform(get("/user").queryParam("email", email))
                .andExpect(content().json(jsonSerialize(expected)));
    }

    @Test
    void createUserTest() {

        var user = a(KeycloakUserDTO.class);
        String serviceResponse = "keycloak-response";
        String expected = "keycloak-response";

        when(keycloakUserService.createUser(user)).thenReturn(serviceResponse);

        var response = userController.createUser(user);

        assertThat(response).isEqualTo(expected);
    }

    @Test
    void createUserAsT1() {
        var keycloakUserDTO = a(KeycloakUserDTO.class);
        userController.createUserAsT1(keycloakUserDTO);
        verify(keycloakUserService).createUser(keycloakUserDTO);
    }

    @Test
    void whenRolesEmpty_createUserAsT1_shoudThrowException() {
        var keycloakUserDTO = a(KeycloakUserDTO.class).setRoles(new ArrayList<>());

        var ex = catchException(() -> userController.createUserAsT1(keycloakUserDTO));

        Assertions.assertThat(ex).isNotNull().isInstanceOf(EmptyRolesException.class);
    }

    @Test
    void getRoles() {
        var userId = UUID.randomUUID();
        userController.getRoles(userId.toString());
        verify(keycloakUserService).getUserRoles(userId.toString());
    }

    @Test
    void getUserByUuid() {
        var userId = UUID.randomUUID();
        userController.getUserByUuid(userId.toString());
        verify(keycloakUserService).getUserByUuid(userId.toString());
    }

    @Test
    void updateUser() {
        var userId = UUID.randomUUID();
        var keycloakUserDTO = a(KeycloakUserDTO.class);
        userController.updateUser(userId.toString(), keycloakUserDTO);
        verify(keycloakUserService).updateUser(userId.toString(), keycloakUserDTO);
    }

    @Test
    void updateUserRoles() {
        var userId = UUID.randomUUID();
        var userRolesElement = a(String.class);
        var userRoles = List.of(userRolesElement);
        userController.updateUserRoles(userId.toString(), userRoles);
        verify(keycloakUserService).updateUserRoles(userId.toString(), userRoles);
    }

    @Test
    void deleteUser() {
        UUID userId = UUID.randomUUID();
        userController.deleteUser(userId.toString());
        verify(keycloakUserService).deleteUser(userId.toString());
    }

    @Test
    void search() {
        var keycloakUserFilter = a(KeycloakUserFilter.class);
        userController.search(keycloakUserFilter);
        verify(keycloakUserService).search(keycloakUserFilter);
    }
}
