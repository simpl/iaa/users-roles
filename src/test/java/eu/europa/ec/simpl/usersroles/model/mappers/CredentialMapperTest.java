package eu.europa.ec.simpl.usersroles.model.mappers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import eu.europa.ec.simpl.usersroles.mappers.CredentialMapper;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

class CredentialMapperTest {

    private CredentialMapper credentialMapper;

    @BeforeEach
    void setup() {
        credentialMapper = Mappers.getMapper(CredentialMapper.class);
    }

    MultipartFile file = new MockMultipartFile("test.txt", "test".getBytes());

    @Test
    void toEntityTest() {
        var entity = credentialMapper.toEntity(file);
        assertThat(entity).isNotNull();
        entity = credentialMapper.toEntity(null);
        assertThat(entity).isNull();
    }

    @Test
    void getBytes_withValidFile_shouldReturnBytes() throws IOException {
        byte[] bytes = credentialMapper.getBytes(file);
        assertNotNull(bytes);
    }
}
