package eu.europa.ec.simpl.usersroles.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.IdentityAttributesApi;
import eu.europa.ec.simpl.api.authenticationprovider.v1.model.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.usersroles.filters.IdentityAttributeWithOwnershipFilter;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeWithOwnershipMapperImpl;
import eu.europa.ec.simpl.usersroles.mappers.SearchIdentityAttributesWithOwnershipFilterParameterMapperImpl;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import({
    IdentityAttributeController.class,
    SearchIdentityAttributesWithOwnershipFilterParameterMapperImpl.class,
    IdentityAttributeWithOwnershipMapperImpl.class,
})
class IdentityAttributeControllerTest {

    @MockitoBean(answers = Answers.RETURNS_DEEP_STUBS)
    IdentityAttributesApi identityAttributesApi;

    @Autowired
    IdentityAttributeController controller;

    @Test
    void search() {
        var filter = an(IdentityAttributeWithOwnershipFilter.class);
        var pageRequest = PageRequest.of(1, 100);
        var searchResult = identityAttributesApi.searchIdentityAttributesWithOwnership(any(), any(), any(), any());
        List<IdentityAttributeWithOwnershipDTO> searchContent = List.of(a(IdentityAttributeWithOwnershipDTO.class));
        given(searchResult.getContent()).willReturn(searchContent);

        // When
        controller.search(filter, pageRequest);
        // Then
        verify(identityAttributesApi)
                .searchIdentityAttributesWithOwnership(
                        eq(pageRequest.getPageNumber()), eq(pageRequest.getPageSize()), any(), any());
    }
}
