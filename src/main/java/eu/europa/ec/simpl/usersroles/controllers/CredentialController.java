package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.CredentialsApi;
import eu.europa.ec.simpl.common.exchanges.usersroles.CredentialExchange;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.CredentialDTO;
import eu.europa.ec.simpl.usersroles.mappers.CredentialMapper;
import eu.europa.ec.simpl.usersroles.mappers.ParticipantMapper;
import eu.europa.ec.simpl.usersroles.services.CredentialService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@Log4j2
@RestController
public class CredentialController implements CredentialExchange {

    private final CredentialsApi credentialsApi;
    private final CredentialMapper credentialMapper;
    private final ParticipantMapper participantMapper;
    private final CredentialService credentialService;

    public CredentialController(
            CredentialMapper credentialMapper,
            ParticipantMapper participantMapper,
            CredentialsApi credentialsApi,
            CredentialService credentialService) {
        this.credentialsApi = credentialsApi;
        this.participantMapper = participantMapper;
        this.credentialMapper = credentialMapper;
        this.credentialService = credentialService;
    }

    @Override
    public long uploadCredential(MultipartFile file) {
        return credentialsApi.uploadCredential(file);
    }

    @Override
    public boolean hasCredential() {
        try {
            credentialsApi.getCredential();
            return true;
        } catch (HttpClientErrorException.NotFound ignored) {
            log.info("No active credentials found in agent");
            return false;
        }
    }

    @Override
    public void delete() {
        credentialsApi.delete();
    }

    @Override
    public StreamingResponseBody downloadInstalledCredentials() {
        return credentialsApi.downloadInstalledCredentials();
    }

    @Override
    public StreamingResponseBody downloadLocalCredentials() {
        return outputStream -> outputStream.write(credentialService.getCredential());
    }

    @Override
    public CredentialDTO getPublicKey() {
        return credentialMapper.toCredentialDTO(credentialsApi.getPublicKey());
    }

    @Override
    public ParticipantDTO getMyParticipantId() {
        return participantMapper.toParticipantDTO(credentialsApi.getMyParticipantId());
    }

    @Override
    public ParticipantDTO getCredentialId() {
        return participantMapper.toParticipantDTO(credentialsApi.getCredentialId());
    }
}
