package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.AgentsApi;
import eu.europa.ec.simpl.common.exchanges.usersroles.AgentControllerExchange;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.EchoDTO;
import eu.europa.ec.simpl.usersroles.mappers.EchoMapper;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeMapper;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeWithOwnershipMapper;
import eu.europa.ec.simpl.usersroles.mappers.ParticipantWithIdentityAttributesMapper;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AgentController implements AgentControllerExchange {
    private final AgentsApi agentsApi;
    private final EchoMapper echoMapper;
    private final IdentityAttributeWithOwnershipMapper identityAttributeWithOwnershipMapper;
    private final IdentityAttributeMapper identityAttributeMapper;
    private final ParticipantWithIdentityAttributesMapper participantWithIdentityAttributesMapper;

    public AgentController(
            AgentsApi agentsApi,
            EchoMapper echoMapper,
            IdentityAttributeWithOwnershipMapper identityAttributeWithOwnershipMapper,
            IdentityAttributeMapper identityAttributeMapper,
            ParticipantWithIdentityAttributesMapper participantWithIdentityAttributesMapper) {
        this.agentsApi = agentsApi;
        this.echoMapper = echoMapper;
        this.identityAttributeWithOwnershipMapper = identityAttributeWithOwnershipMapper;
        this.identityAttributeMapper = identityAttributeMapper;
        this.participantWithIdentityAttributesMapper = participantWithIdentityAttributesMapper;
    }

    @Override
    public EchoDTO echo() {
        return echoMapper.toDto(agentsApi.echo());
    }

    @Override
    public List<IdentityAttributeWithOwnershipDTO> getIdentityAttributesWithOwnership() {
        return identityAttributeWithOwnershipMapper.toListDto(agentsApi.getIdentityAttributesWithOwnership());
    }

    @Override
    public List<IdentityAttributeDTO> getParticipantIdentityAttributes(@PathVariable String credentialId) {
        return identityAttributeMapper.toListDto(agentsApi.getParticipantIdentityAttributes(credentialId));
    }

    @Override
    public ParticipantWithIdentityAttributesDTO ping(@RequestParam String fqdn) {
        return participantWithIdentityAttributesMapper.toDto(agentsApi.pingAgent(fqdn));
    }
}
