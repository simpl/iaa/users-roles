package eu.europa.ec.simpl.usersroles.services;

import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleScopeResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;

public interface KeycloakService {

    RealmResource getAppRealm();

    UsersResource getAppUsers();

    RoleScopeResource getClientRoleScope(String userId);

    RolesResource getClientRoles();

    String getClientUUID();
}
