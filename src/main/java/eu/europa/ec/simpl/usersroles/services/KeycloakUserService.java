package eu.europa.ec.simpl.usersroles.services;

import eu.europa.ec.simpl.common.filters.KeycloakUserFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.common.model.validators.CreateOperation;
import eu.europa.ec.simpl.usersroles.exceptions.KeycloakException;
import jakarta.validation.Valid;
import jakarta.ws.rs.ClientErrorException;
import java.util.List;
import java.util.UUID;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.validation.annotation.Validated;

public interface KeycloakUserService {
    @Validated(CreateOperation.class)
    String createUser(@Valid KeycloakUserDTO userDTO);

    KeycloakUserDTO getUserByEmail(String email);

    void importUsers(List<KeycloakUserDTO> users);

    void importUser(KeycloakUserDTO user);

    RoleRepresentation getRole(String roleName);

    RoleRepresentation getRole(UUID uuid);

    boolean isRolePresent(String roleName) throws KeycloakException;

    List<RoleRepresentation> getClientRoles();

    void importRoles(List<KeycloakRoleDTO> roles);

    void updateRole(RoleDTO roleDTO) throws ClientErrorException;

    void deleteRole(String roleName) throws ClientErrorException;

    void importRole(KeycloakRoleDTO role) throws ClientErrorException;

    List<KeycloakRoleDTO> getUserRoles(String uuid);

    KeycloakUserDTO getUserByUuid(String uuid);

    void updateUser(String uuid, KeycloakUserDTO userDTO);

    void deleteUser(String uuid);

    void updateUserRoles(String uuid, List<String> userRoles);

    List<KeycloakUserDTO> search(KeycloakUserFilter filter);

    List<RoleRepresentation> getRoleList();

    void logout();
}
