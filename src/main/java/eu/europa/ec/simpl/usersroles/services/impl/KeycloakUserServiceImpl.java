package eu.europa.ec.simpl.usersroles.services.impl;

import eu.europa.ec.simpl.common.filters.KeycloakUserFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.usersroles.exceptions.KeycloakException;
import eu.europa.ec.simpl.usersroles.mappers.KeycloakMapper;
import eu.europa.ec.simpl.usersroles.services.KeycloakService;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;
import lombok.extern.log4j.Log4j2;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class KeycloakUserServiceImpl implements KeycloakUserService {

    private static final Pattern LAST_PATH_LOCATION_PATTERN = Pattern.compile(".{0,2048}/(.+)");

    private final KeycloakService keycloakService;
    private final KeycloakMapper keycloakMapper;
    private final JwtService jwtService;

    public KeycloakUserServiceImpl(
            KeycloakService keycloakService, KeycloakMapper keycloakMapper, JwtService jwtService) {
        this.keycloakService = keycloakService;
        this.keycloakMapper = keycloakMapper;
        this.jwtService = jwtService;
    }

    @Override
    public String createUser(KeycloakUserDTO userDTO) {
        var userRepresentation = keycloakMapper.toRepresentation(userDTO);
        var userId = createUser(userRepresentation);
        var requestRoles = userDTO.getRoles().stream()
                .map(role -> keycloakService.getAppRealm().roles().get(role))
                .map(RoleResource::toRepresentation)
                .toList();

        keycloakService.getAppUsers().get(userId).roles().realmLevel().add(requestRoles);
        return userId;
    }

    private String createUser(UserRepresentation userRepresentation) {
        // TODO Handle error(s)
        try (var response = keycloakService.getAppUsers().create(userRepresentation)) {
            if (Objects.equals(HttpStatus.valueOf(response.getStatus()), HttpStatus.CREATED)) {
                var headerString = response.getHeaderString(HttpHeaders.LOCATION);
                var userId = getUserIdFromLocation(headerString);
                log.info("createUser: user created [{}]", userRepresentation.getRawAttributes());
                return userId;
            }
            throw new KeycloakException(response);
        }
    }

    private static String getUserIdFromLocation(String location) {
        var matcher = LAST_PATH_LOCATION_PATTERN.matcher(location);
        if (!matcher.matches()) {
            throw new IllegalStateException("Invalid location from keycloak");
        }
        return matcher.group(1);
    }

    @Override
    public KeycloakUserDTO getUserByEmail(String email) {
        log.info("getUserById: retrieving user with email [{}]", email);
        try {
            var userRepresentation = keycloakService.getAppUsers().searchByEmail(email, true).stream()
                    .findFirst()
                    .orElseThrow(NotFoundException::new);
            var roleList =
                    keycloakService
                            .getAppRealm()
                            .users()
                            .get(userRepresentation.getId())
                            .roles()
                            .realmLevel()
                            .listAll()
                            .stream()
                            .map(RoleRepresentation::getName)
                            .toList();
            return keycloakMapper.toDto(userRepresentation, roleList);
        } catch (ClientErrorException e) {
            log.error("getUserById: error retrieving user with email [{}]", email);
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public void importUsers(List<KeycloakUserDTO> users) {
        users.forEach(this::importUser);
    }

    @Override
    public void importUser(KeycloakUserDTO user) {
        log.info("importUser: importing user [{}]", user.getUsername());
        var userId = createUser(user);
        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            updateUserRoles(userId, user.getRoles());
        }
    }

    @Override
    public RoleRepresentation getRole(String roleName) {
        try {
            log.info("getRole: retrieving role with name [{}]", roleName);
            return keycloakService.getAppRealm().roles().get(roleName).toRepresentation();
        } catch (ClientErrorException e) {
            log.error("getRole: error retrieving role with name [{}]", roleName);
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public RoleRepresentation getRole(UUID uuid) {
        try {
            log.info("Retrieving role with UUID [{}]", uuid);
            return keycloakService.getAppRealm().rolesById().getRole(uuid.toString());
        } catch (NotFoundException e) {
            log.info("Role with UUID [{}] does not exist", uuid);
            throw e;
        } catch (ClientErrorException e) {
            log.error("getRole: error retrieving role with id [{}]", uuid.toString());
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public boolean isRolePresent(String roleName) throws KeycloakException {
        try {
            log.info("Checking existence of role [{}]", roleName);
            RoleResource roleResource = keycloakService.getAppRealm().roles().get(roleName);
            roleResource.toRepresentation();
            return true;
        } catch (NotFoundException e) {
            log.info("Role [{}] does not exist", roleName);
            return false;
        } catch (ClientErrorException e) {
            log.error("Error while verifying role [{}]", roleName);
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public List<RoleRepresentation> getClientRoles() {
        try {
            log.info("Retrieving role list.");
            return keycloakService.getClientRoles().list();
        } catch (ClientErrorException e) {
            log.error("Error while retrieving role list");
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public void importRoles(List<KeycloakRoleDTO> roles) {
        roles.stream().map(keycloakMapper::toRepresentation).forEach(this::addRoleToRealm);
    }

    @Override
    public void updateRole(RoleDTO roleDTO) throws ClientErrorException {
        var realmRoles = keycloakService.getAppRealm().roles();
        var representation = realmRoles.get(roleDTO.getName()).toRepresentation();
        if (roleDTO.getDescription() != null) {
            representation.setDescription(roleDTO.getDescription());
        }
        realmRoles.get(roleDTO.getName()).update(representation);
        log.info("updateRole: role [{}] updated on Keycloak", roleDTO.getName());
    }

    @Override
    public void deleteRole(String roleName) throws ClientErrorException {
        var realmRoles = keycloakService.getAppRealm().roles();
        var role = realmRoles.get(roleName).toRepresentation();
        if (role != null) {
            realmRoles.deleteRole(role.getName());
            log.info("deleteRole: role [{}] deleted from Keycloak", roleName);
        }
    }

    private void addRoleToRealm(RoleRepresentation role) {
        try {
            keycloakService.getAppRealm().roles().create(role);
            log.info("addRoleToRealm: added role [{}] to realm", role.getName());
        } catch (ClientErrorException e) {
            log.error("addRoleToRealm: error adding role [{}] to realm", role.getName());
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public void importRole(KeycloakRoleDTO role) throws ClientErrorException {
        var roleToSave = keycloakMapper.toRepresentation(role);
        keycloakService.getAppRealm().roles().create(roleToSave);
        log.info("importRole: added role [{}]", roleToSave.getName());
    }

    @Override
    public List<KeycloakRoleDTO> getUserRoles(String uuid) {
        try {
            return keycloakService.getAppRealm().users().get(uuid).roles().realmLevel().listAll().stream()
                    .map(keycloakMapper::toDto)
                    .toList();
        } catch (ClientErrorException e) {
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public KeycloakUserDTO getUserByUuid(String uuid) {
        try {
            var representation = keycloakService.getAppUsers().get(uuid).toRepresentation();
            var roleList =
                    keycloakService
                            .getAppRealm()
                            .users()
                            .get(representation.getId())
                            .roles()
                            .realmLevel()
                            .listAll()
                            .stream()
                            .map(RoleRepresentation::getName)
                            .toList();
            return keycloakMapper.toDto(representation, roleList);
        } catch (ClientErrorException e) {
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public void updateUser(String uuid, KeycloakUserDTO userDTO) {
        try {
            var userResource = keycloakService.getAppUsers().get(uuid);
            var representation = userResource.toRepresentation();
            keycloakMapper.updateEntity(userDTO, representation);
            userResource.update(representation);
            updateUserRoles(uuid, userDTO.getRoles());
        } catch (ClientErrorException e) {
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public void deleteUser(String uuid) {
        try (var response = keycloakService.getAppUsers().delete(uuid)) {
            if (response.getStatus() != Response.Status.NO_CONTENT.getStatusCode()) {
                throw new KeycloakException(response);
            }
        }
    }

    @Override
    public void updateUserRoles(String uuid, List<String> userRoles) {
        try {
            log.info("addRolesToUser: adding roles {} to user {}", userRoles, uuid);
            var roles = userRoles.stream().distinct().map(this::getRole).toList();
            keycloakService.getAppRealm().users().get(uuid).roles().realmLevel().add(roles);
        } catch (ClientErrorException e) {
            log.error("addRolesToUser: error adding roles {} to user {}", userRoles, uuid, e);
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public List<KeycloakUserDTO> search(KeycloakUserFilter filter) {
        try {
            var representations = keycloakService
                    .getAppUsers()
                    .search(
                            filter.getUsername(),
                            filter.getFirstName(),
                            filter.getLastName(),
                            filter.getEmail(),
                            filter.getFirst(),
                            filter.getMax(),
                            true,
                            false,
                            false);

            return representations.stream()
                    .map(repr -> {
                        var roleList =
                                keycloakService
                                        .getAppRealm()
                                        .users()
                                        .get(repr.getId())
                                        .roles()
                                        .realmLevel()
                                        .listAll()
                                        .stream()
                                        .map(RoleRepresentation::getName)
                                        .toList();
                        return keycloakMapper.toDto(repr, roleList);
                    })
                    .toList();
        } catch (ClientErrorException e) {
            throw new KeycloakException(e.getResponse());
        }
    }

    @Override
    public List<RoleRepresentation> getRoleList() {
        return keycloakService.getAppRealm().roles().list();
    }

    @Override
    public void logout() {
        var sid = jwtService.getClaim("sid");
        try {
            keycloakService.getAppRealm().deleteSession(sid, false);
        } catch (ClientErrorException e) {
            throw new KeycloakException(e.getResponse());
        }
    }
}
