package eu.europa.ec.simpl.usersroles.services;

import java.security.PrivateKey;

public interface KeyPairService {
    PrivateKey getPrivateKey();
}
