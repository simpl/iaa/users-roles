package eu.europa.ec.simpl.usersroles.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class EmptyRolesException extends StatusException {

    public EmptyRolesException(HttpStatus status, String message) {
        super(status, message);
    }
}
