package eu.europa.ec.simpl.usersroles.services.impl;

import eu.europa.ec.simpl.common.filters.RoleFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeRoles;
import eu.europa.ec.simpl.usersroles.exceptions.KeycloakRoleException;
import eu.europa.ec.simpl.usersroles.exceptions.RoleDuplicationException;
import eu.europa.ec.simpl.usersroles.exceptions.RoleNotFoundException;
import eu.europa.ec.simpl.usersroles.exceptions.RoleUpdateException;
import eu.europa.ec.simpl.usersroles.mappers.RoleMapper;
import eu.europa.ec.simpl.usersroles.repositories.IdentityAttributeRolesRepository;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import eu.europa.ec.simpl.usersroles.services.RoleService;
import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.NotFoundException;
import java.beans.PropertyDescriptor;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
public class RoleServiceImpl implements RoleService {
    private final IdentityAttributeRolesRepository identityAttributeRolesRepository;
    private final RoleMapper roleMapper;
    private final KeycloakUserService keycloakService;

    public RoleServiceImpl(
            IdentityAttributeRolesRepository identityAttributeRolesRepository,
            RoleMapper roleMapper,
            KeycloakUserService keycloakService) {
        this.identityAttributeRolesRepository = identityAttributeRolesRepository;
        this.roleMapper = roleMapper;
        this.keycloakService = keycloakService;
    }

    @Override
    @Transactional
    public void replaceIdentityAttributes(UUID roleId, List<String> identityAttributesCode) {
        var roleRepresentation = getRoleRepresentation(roleId);
        var roleName = roleRepresentation.getName();
        identityAttributeRolesRepository.deleteByRoleName(roleName);
        identityAttributeRolesRepository.flush();
        var savedAttributes = identityAttributeRolesRepository.saveAll(identityAttributesCode.stream()
                .map(idaCode -> new IdentityAttributeRoles()
                        .setRoleName(roleName)
                        .setIdaCode(idaCode)
                        .setEnabled(true))
                .toList());

        var savedCodes =
                savedAttributes.stream().map(IdentityAttributeRoles::getIdaCode).collect(Collectors.joining(","));

        log.info("Assigned identity attributes [{}] to role {} ", savedCodes, roleName);
    }

    @Override
    @Transactional
    public void removeAttributeForRole(String attributeCode, UUID roleId) {
        var roleRepresentation = getRoleRepresentation(roleId);
        var roleName = roleRepresentation.getName();
        if (identityAttributeRolesRepository.deleteByRoleNameAndIdaCode(roleName, attributeCode) == 0) {
            log.warn("Identity Attribute {} is not assigned to role {}.", attributeCode, roleName);
        }
    }

    @Override
    @Transactional
    public void duplicateIdentityAttributeToAnOtherRole(UUID sourceRoleId, UUID destinationRoleId) {
        var sourceRole = getRoleRepresentation(sourceRoleId);
        var sourceRoleIDAs = getEnabledAttrs(sourceRole);

        var destinationRole = getRoleRepresentation(destinationRoleId);
        var destinationRoleIDAs = getEnabledAttrs(destinationRole);

        var missingAttributes = getMissingAttributes(sourceRoleIDAs, destinationRoleIDAs);

        missingAttributes.forEach(idaCode -> identityAttributeRolesRepository.save(new IdentityAttributeRoles()
                .setRoleName(destinationRole.getName())
                .setIdaCode(idaCode)
                .setEnabled(true)));
    }

    /**
     * This method compares the identity attribute codes between two roles and identifies
     * which attributes are present in the source role but absent in the destination role.
     *
     * @param sourceRoleIDAs      A list of IdentityAttributeRoles associated with the source role.
     * @param destinationRoleIDAs A list of IdentityAttributeRoles associated with the destination role.
     * @return A list of String representing the identity attribute codes that are present in the source role
     *         but missing from the destination role.
     */
    private List<String> getMissingAttributes(
            List<IdentityAttributeRoles> sourceRoleIDAs, List<IdentityAttributeRoles> destinationRoleIDAs) {
        var sourceIdaCodes =
                sourceRoleIDAs.stream().map(IdentityAttributeRoles::getIdaCode).toList();

        var destinationIdaCodes = destinationRoleIDAs.stream()
                .map(IdentityAttributeRoles::getIdaCode)
                .toList();

        return sourceIdaCodes.stream()
                .filter(idaCode -> !destinationIdaCodes.contains(idaCode))
                .toList();
    }

    @Override
    public List<String> findIdentityAttributesForUser(List<String> roleNames) {
        log.info("RoleService.findIdentityAttributesForUser with rolesName {}", roleNames);
        return identityAttributeRolesRepository.findDistinctEnabledIdaCodesByRoleNames(roleNames);
    }

    @Override
    public RoleDTO findById(UUID roleId) {
        log.info("retrieving role with id {}", roleId);
        var role = getRoleRepresentation(roleId);
        var idas = getEnabledAttrs(role);
        return roleMapper.toDto(role, idas);
    }

    @Override
    @Transactional
    public void preAssignIdentityAttributesToRole(List<String> idaCodes, String roleName) {
        var mappings = idaCodes.stream()
                .map(idaCode -> new IdentityAttributeRoles()
                        .setRoleName(roleName)
                        .setIdaCode(idaCode)
                        .setEnabled(false))
                .toList();
        identityAttributeRolesRepository.saveAll(mappings);
    }

    @Override
    @Transactional
    public RoleDTO create(KeycloakRoleDTO roleDTO) {
        log.info("creating role {}", roleDTO.name());
        if (keycloakService.isRolePresent(roleDTO.name())) {
            throw new RoleDuplicationException();
        }
        RoleRepresentation keycloakRole;
        try {
            keycloakService.importRole(roleDTO);
            keycloakRole = keycloakService.getRole(roleDTO.name());
        } catch (ClientErrorException e) {
            throw new KeycloakRoleException(e.getMessage());
        }
        log.info("role {} stored successfully", keycloakRole.getName());
        return roleMapper.toDto(keycloakRole, Collections.emptyList());
    }

    @Override
    @Transactional
    public RoleDTO update(RoleDTO roleDTO) {
        log.info("updating role {}", roleDTO.getName());
        var role = getRoleRepresentation(roleDTO.getId());
        var idas = getEnabledAttrs(role);
        var retrievedRole = roleMapper.toDto(role, idas);

        if (!Objects.equals(retrievedRole.getName(), roleDTO.getName())) {
            throw new RoleUpdateException(HttpStatus.FORBIDDEN, "field 'name' cannot be modified");
        }

        try {
            keycloakService.updateRole(roleDTO);
        } catch (ClientErrorException e) {
            throw new KeycloakRoleException(e.getMessage());
        }

        var updatedRole = getRoleRepresentation(roleDTO.getId());
        return roleMapper.toDto(updatedRole, idas);
    }

    @Override
    @Transactional
    public void delete(UUID roleId) {
        var roleRepresentation = getRoleRepresentation(roleId);
        var roleName = roleRepresentation.getName();
        identityAttributeRolesRepository.disableMappingByRoleName(roleName);

        log.info("deleting role {}", roleName);
        try {
            keycloakService.deleteRole(roleName);
        } catch (ClientErrorException e) {
            throw new KeycloakRoleException(e.getMessage());
        }
    }

    @Override
    public Page<RoleDTO> search(RoleFilter request, Pageable pageable) {
        List<RoleRepresentation> roleList = keycloakService.getRoleList();

        var filteredRoles = roleList.stream()
                .map(role -> roleMapper.toDto(role, getEnabledAttrs(role)))
                .filter(roleDTO -> shouldMatchFilter(roleDTO, request))
                .sorted(toComparator(pageable.getSort()))
                .skip(pageable.getOffset())
                .limit(pageable.getPageSize())
                .toList();

        var totalElements = roleList.stream()
                .map(role -> roleMapper.toDto(role, getEnabledAttrs(role)))
                .filter(roleDTO -> shouldMatchFilter(roleDTO, request))
                .count();

        return new PageImpl<>(filteredRoles, pageable, totalElements);
    }

    private Comparator<? super RoleDTO> toComparator(Sort sort) {
        return sort.stream()
                .flatMap(this::toComparator)
                .reduce(Comparator::thenComparing)
                .orElse(unsortedComparator());
    }

    private Stream<Comparator<RoleDTO>> toComparator(Sort.Order order) {
        try {
            return Optional.ofNullable(getPropertyDescriptor(order))
                    .flatMap(property -> toComparator(property, order.getDirection()))
                    .stream();
        } catch (BeansException e) {
            return Stream.empty();
        }
    }

    private static PropertyDescriptor getPropertyDescriptor(Sort.Order order) {
        return BeanUtils.getPropertyDescriptor(RoleDTO.class, order.getProperty());
    }

    private static Optional<Comparator<RoleDTO>> toComparator(PropertyDescriptor property, Sort.Direction direction) {
        if (Comparable.class.isAssignableFrom(property.getPropertyType())) {
            return Optional.of(buildComparator(property, direction));
        } else {
            return Optional.empty();
        }
    }

    private static Comparator<RoleDTO> buildComparator(PropertyDescriptor property, Sort.Direction direction) {
        var comparator = Comparator.<RoleDTO, Comparable<Object>>comparing(o -> get(o, property));
        if (direction == Sort.Direction.DESC) {
            comparator = comparator.reversed();
        }
        return comparator;
    }

    private static Comparator<RoleDTO> unsortedComparator() {
        return (o1, o2) -> 0;
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    private static Comparable<Object> get(RoleDTO o, PropertyDescriptor property) {
        return (Comparable<Object>) property.getReadMethod().invoke(o);
    }

    @Override
    public void importRoles(List<KeycloakRoleDTO> roles) {
        keycloakService.importRoles(roles);
    }

    private RoleRepresentation getRoleRepresentation(UUID roleId) {
        RoleRepresentation role;
        try {
            role = keycloakService.getRole(roleId);
        } catch (NotFoundException e) {
            throw new RoleNotFoundException(roleId, String.format("Role %s not found", roleId));
        }
        return role;
    }

    private List<IdentityAttributeRoles> getEnabledAttrs(RoleRepresentation role) {
        return identityAttributeRolesRepository.findByRoleNameAndEnabledTrue(role.getName());
    }

    private static boolean shouldMatchFilter(RoleDTO roleDTO, RoleFilter filter) {
        if (filter == null) {
            return true;
        }
        var matches = true;

        if (StringUtils.isNotBlank(filter.getName())) {
            matches &= StringUtils.containsIgnoreCase(roleDTO.getName(), filter.getName());
        }
        if (StringUtils.isNotBlank(filter.getDescription())) {
            matches &= StringUtils.isNotBlank(roleDTO.getDescription())
                    && StringUtils.containsIgnoreCase(roleDTO.getDescription(), filter.getDescription());
        }
        if (StringUtils.isNotBlank(filter.getAttributeName())) {
            matches &= roleDTO.getAssignedIdentityAttributes().stream()
                    .anyMatch(ia -> StringUtils.equals(ia, filter.getAttributeName()));
        }
        return matches;
    }
}
