package eu.europa.ec.simpl.usersroles.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class RoleUpdateException extends StatusException {
    public RoleUpdateException(HttpStatus status, String message) {
        super(status, message);
    }
}
