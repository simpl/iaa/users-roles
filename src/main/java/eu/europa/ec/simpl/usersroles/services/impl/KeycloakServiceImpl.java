package eu.europa.ec.simpl.usersroles.services.impl;

import static org.keycloak.crypto.KeyUse.SIG;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.constants.Topics;
import eu.europa.ec.simpl.common.exchanges.mtls.AuthorityExchange;
import eu.europa.ec.simpl.common.messaging.OnMessageEvent;
import eu.europa.ec.simpl.events.authenticationprovider.v1.CredentialUpdatedEvent;
import eu.europa.ec.simpl.usersroles.configurations.keycloak.KeycloakProperties;
import eu.europa.ec.simpl.usersroles.services.KeycloakService;
import jakarta.ws.rs.NotFoundException;
import java.util.Objects;
import java.util.function.Supplier;
import lombok.SneakyThrows;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleScopeResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.KeysMetadataRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class KeycloakServiceImpl implements KeycloakService {

    private final Supplier<AuthorityExchange> authorityExchange;
    private final KeycloakProperties properties;
    private final Keycloak keycloak;
    private final String topicPrefix;
    private final ObjectMapper objectMapper;

    public KeycloakServiceImpl(
            Supplier<AuthorityExchange> authorityExchange,
            Keycloak keycloak,
            KeycloakProperties properties,
            @Value("${simpl.kafka.topic.prefix}") String topicPrefix,
            ObjectMapper objectMapper) {
        this.authorityExchange = authorityExchange;
        this.properties = properties;
        this.keycloak = keycloak;
        this.topicPrefix = topicPrefix;
        this.objectMapper = objectMapper;
    }

    @EventListener
    public void onMessage(OnMessageEvent event) {
        if (event.getTopic()
                .equals("%s%s".formatted(topicPrefix, Topics.AUTHENTICATION_PROVIDER_CREDENTIAL_UPDATED_EVENT))) {
            var payload = parseJson(event.getMessage());
            if (Boolean.FALSE.equals(payload.getDeleted())) {
                authorityExchange.get().sendTierOnePublicKey(getKeycloakSignPublicKey());
            }
        }
    }

    @SneakyThrows
    private CredentialUpdatedEvent parseJson(String message) {
        return objectMapper.readValue(message, CredentialUpdatedEvent.class);
    }

    private String getKeycloakSignPublicKey() {
        return getAppRealm().keys().getKeyMetadata().getKeys().stream()
                .filter(k -> Objects.equals(k.getUse(), SIG))
                .map(KeysMetadataRepresentation.KeyMetadataRepresentation::getPublicKey)
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public RealmResource getAppRealm() {
        return keycloak.realm(properties.app().realm());
    }

    @Override
    public UsersResource getAppUsers() {
        return getAppRealm().users();
    }

    @Override
    public RoleScopeResource getClientRoleScope(String userId) {
        return getAppUsers().get(userId).roles().clientLevel(getClientUUID());
    }

    @Override
    public RolesResource getClientRoles() {
        return getAppRealm().clients().get(getClientUUID()).roles();
    }

    @Override
    public String getClientUUID() {
        return getAppRealm().clients().findByClientId(properties.app().clientId()).stream()
                .findFirst()
                .map(ClientRepresentation::getId)
                .orElseThrow(NotFoundException::new);
    }
}
