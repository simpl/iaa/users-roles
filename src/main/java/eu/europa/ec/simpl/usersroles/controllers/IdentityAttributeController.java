package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.authenticationprovider.v1.exchanges.IdentityAttributesApi;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.common.utils.SortUtil;
import eu.europa.ec.simpl.usersroles.filters.IdentityAttributeWithOwnershipFilter;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeWithOwnershipMapper;
import eu.europa.ec.simpl.usersroles.mappers.SearchIdentityAttributesWithOwnershipFilterParameterMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("identity-attribute")
@Slf4j
public class IdentityAttributeController {

    private final IdentityAttributesApi identityAttributesApi;
    private final SearchIdentityAttributesWithOwnershipFilterParameterMapper
            searchIdentityAttributesWithOwnershipFilterParameterMapper;
    private final IdentityAttributeWithOwnershipMapper identityAttributeWithOwnershipMapper;

    public IdentityAttributeController(
            IdentityAttributesApi identityAttributesApi,
            SearchIdentityAttributesWithOwnershipFilterParameterMapper
                    searchIdentityAttributesWithOwnershipFilterParameterMapper,
            IdentityAttributeWithOwnershipMapper identityAttributeWithOwnershipMapper) {
        this.identityAttributesApi = identityAttributesApi;
        this.searchIdentityAttributesWithOwnershipFilterParameterMapper =
                searchIdentityAttributesWithOwnershipFilterParameterMapper;
        this.identityAttributeWithOwnershipMapper = identityAttributeWithOwnershipMapper;
    }

    @Operation(
            summary = "Search identity attributes with ownership",
            description =
                    "Searches for identity attributes with ownership based on the provided filter and pagination settings",
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully retrieved the identity attributes"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
            })
    @GetMapping("search")
    public Page<IdentityAttributeWithOwnershipDTO> search(
            @ParameterObject IdentityAttributeWithOwnershipFilter filter,
            @PageableDefault(sort = "id") @ParameterObject Pageable pageable) {
        var page = pageable.getPageNumber();
        var size = pageable.getPageSize();
        var sort = SortUtil.toStringList(pageable.getSort());
        var authenticantionProviderFileter = searchIdentityAttributesWithOwnershipFilterParameterMapper.toDTO(filter);
        var response = identityAttributesApi.searchIdentityAttributesWithOwnership(
                page, size, sort, authenticantionProviderFileter);
        Long total = response.getPage().getTotalElements();
        List<IdentityAttributeWithOwnershipDTO> content =
                identityAttributeWithOwnershipMapper.toListDto(response.getContent());
        return new PageImpl<>(content, pageable, total);
    }
}
