package eu.europa.ec.simpl.usersroles.exchanges;

import eu.europa.ec.simpl.common.constants.SimplHeaders;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PatchExchange;

@HttpExchange
public interface PublicKeyExchange {
    @PatchExchange(value = "/mtls/public-key", contentType = MediaType.TEXT_PLAIN_VALUE)
    ParticipantDTO sendTierOnePublicKey(
            @RequestHeader(SimplHeaders.CREDENTIAL_ID) String credentialId, @RequestBody String tierOnePublicKey);
}
