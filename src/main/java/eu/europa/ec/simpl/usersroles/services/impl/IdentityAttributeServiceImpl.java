package eu.europa.ec.simpl.usersroles.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.constants.Topics;
import eu.europa.ec.simpl.common.messaging.OnMessageEvent;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.events.usersroles.v1.AssignedIdentityAttributesUpdatedEvent;
import eu.europa.ec.simpl.usersroles.filters.IdentityAttributeWithOwnershipFilter;
import eu.europa.ec.simpl.usersroles.mappers.IdentityAttributeWithOwnershipMapper;
import eu.europa.ec.simpl.usersroles.repositories.IdentityAttributeRolesRepository;
import eu.europa.ec.simpl.usersroles.repositories.IdentityAttributeWithOwnershipRepository;
import eu.europa.ec.simpl.usersroles.repositories.specification.IdentityAttributeSpecification;
import eu.europa.ec.simpl.usersroles.services.IdentityAttributeService;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class IdentityAttributeServiceImpl implements IdentityAttributeService {

    private final ObjectMapper objectMapper;
    private final IdentityAttributeWithOwnershipMapper idaMapper;
    private final IdentityAttributeWithOwnershipRepository idaRepository;
    private final IdentityAttributeRolesRepository idaRolesRepository;
    private final Function<IdentityAttributeUpdateHelper.Config, IdentityAttributeUpdateHelper> updateHelperFactory;
    private final String topicPrefix;

    public IdentityAttributeServiceImpl(
            ObjectMapper objectMapper,
            IdentityAttributeWithOwnershipMapper idaMapper,
            IdentityAttributeWithOwnershipRepository idaRepository,
            IdentityAttributeRolesRepository idaRolesRepository,
            @Autowired(required = false)
                    Function<IdentityAttributeUpdateHelper.Config, IdentityAttributeUpdateHelper> updateHelperFactory,
            @Value("${simpl.kafka.topic.prefix}") String topicPrefix) {
        this.objectMapper = objectMapper;
        this.idaMapper = idaMapper;
        this.idaRepository = idaRepository;
        this.idaRolesRepository = idaRolesRepository;
        this.updateHelperFactory = Objects.requireNonNullElse(updateHelperFactory, IdentityAttributeUpdateHelper::new);
        this.topicPrefix = topicPrefix;
    }

    @EventListener
    public void onMessage(OnMessageEvent event) {
        if (event.getTopic()
                .equals("%s%s"
                        .formatted(topicPrefix, Topics.AUTHENTICATION_PROVIDER_IDENTITY_ATTRIBUTES_CHANGED_EVENT))) {

            var assignedIdentityAttributesUpdatedEvent = parseJson(event.getMessage());
            idaRolesRepository.updateAssignments(
                    assignedIdentityAttributesUpdatedEvent.getAssignedIdentityAttributesCodes());
        }
    }

    @SneakyThrows
    private AssignedIdentityAttributesUpdatedEvent parseJson(String message) {
        return objectMapper.readValue(message, AssignedIdentityAttributesUpdatedEvent.class);
    }

    @Override
    public Page<IdentityAttributeWithOwnershipDTO> search(
            IdentityAttributeWithOwnershipFilter request, Pageable pageable) {
        return idaRepository
                .findAll(new IdentityAttributeSpecification(request), pageable)
                .map(idaMapper::toLightDtoWithOwnership);
    }

    @Override
    @Transactional
    public void overwriteIdentityAttributes(List<IdentityAttributeWithOwnershipDTO> identityAttributes) {
        idaRepository.deleteAllInBatch();
        identityAttributes.stream().map(idaMapper::toEntity).forEach(idaRepository::save);
        idaRolesRepository.updateAssignments(identityAttributes.stream()
                .filter(IdentityAttributeWithOwnershipDTO::getAssignedToParticipant)
                .map(ida -> ida.getIdentityAttribute().getCode())
                .toList());
    }

    @Override
    @Transactional
    public void updateAssignedIdentityAttributes(List<IdentityAttributeDTO> idaFromEphemeralProof) {
        var idaFromLocalCopy = idaRepository.findAllFetchParticipantTypes();
        var updateHelper = updateHelperFactory.apply(
                new IdentityAttributeUpdateHelper.Config(idaFromEphemeralProof, idaFromLocalCopy, idaMapper));
        var changes = updateHelper.getIdasChanges();
        if (!changes.isEmpty()) {
            idaRepository.saveAll(changes);
            idaRolesRepository.updateAssignments(idaFromEphemeralProof.stream()
                    .map(IdentityAttributeDTO::getCode)
                    .toList());
        }
    }

    @Override
    public Set<IdentityAttributeDTO> getIdentityAttributesWithCodeIn(List<String> identityAttributesCode) {
        return idaRepository.findAllByCodeIn(identityAttributesCode).stream()
                .map(idaMapper::toLightDto)
                .collect(Collectors.toSet());
    }
}
