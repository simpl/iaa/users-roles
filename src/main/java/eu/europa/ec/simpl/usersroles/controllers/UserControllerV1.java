package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.usersroles.v1.exchanges.UsersApi;
import eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakRoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakUserDTO;
import eu.europa.ec.simpl.usersroles.mappers.UserMapperV1;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class UserControllerV1 implements UsersApi {

    private final UserController controller;
    private final UserMapperV1 mapper;

    public UserControllerV1(UserController controller, UserMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public String createUser(KeycloakUserDTO keycloakUserDTO) {
        return controller.createUser(mapper.toV0(keycloakUserDTO));
    }

    @Override
    public String createUserAsT1(KeycloakUserDTO keycloakUserDTO) {
        return controller.createUserAsT1(mapper.toV0(keycloakUserDTO));
    }

    @Override
    public void deleteUser(String uuid) {
        controller.deleteUser(uuid);
    }

    @Override
    public List<KeycloakRoleDTO> getRoles(String uuid) {
        return mapper.toV1(controller.getRoles(uuid));
    }

    @Override
    public KeycloakUserDTO getUserByUuid(String uuid) {
        return mapper.toV1(controller.getUserByUuid(uuid));
    }

    @Override
    public void importUsers(List<KeycloakUserDTO> keycloakUserDTO) {
        controller.importUsers(mapper.toV0(keycloakUserDTO));
    }

    @Override
    public List<KeycloakUserDTO> search(
            String username, String firstName, String lastName, String email, Integer first, Integer max) {
        return mapper.toListKeycloakUserDTOV1(
                controller.search(mapper.toKeycloakUserFilter(username, firstName, lastName, email, first, max)));
    }

    @Override
    public void updateUser(String uuid, KeycloakUserDTO keycloakUserDTO) {
        controller.updateUser(uuid, mapper.toV0(keycloakUserDTO));
    }

    @Override
    public void updateUserRoles(String uuid, List<String> requestBody) {
        controller.updateUserRoles(uuid, requestBody);
    }
}
