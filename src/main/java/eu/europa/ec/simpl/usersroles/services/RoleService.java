package eu.europa.ec.simpl.usersroles.services;

import eu.europa.ec.simpl.common.filters.RoleFilter;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakRoleDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.RoleDTO;
import eu.europa.ec.simpl.usersroles.exceptions.IdentityAttributeAlreadyAssignedToRoleException;
import eu.europa.ec.simpl.usersroles.exceptions.RoleNotFoundException;
import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public interface RoleService {
    @Transactional
    void replaceIdentityAttributes(UUID roleId, List<String> attributesCode)
            throws RoleNotFoundException, IdentityAttributeAlreadyAssignedToRoleException;

    @Transactional
    void removeAttributeForRole(String attributeCode, UUID roleId);

    @Transactional
    void duplicateIdentityAttributeToAnOtherRole(UUID sourceRoleId, UUID destinationRoleId);

    List<String> findIdentityAttributesForUser(List<String> rolesName);

    RoleDTO findById(UUID roleId);

    void preAssignIdentityAttributesToRole(List<String> idaCodes, String roleName);

    @Transactional
    RoleDTO create(KeycloakRoleDTO roleDTO);

    @Transactional
    RoleDTO update(RoleDTO roleDTO);

    @Transactional
    void delete(UUID roleId);

    Page<RoleDTO> search(RoleFilter request, Pageable pageable);

    void importRoles(@Valid List<KeycloakRoleDTO> roles);
}
