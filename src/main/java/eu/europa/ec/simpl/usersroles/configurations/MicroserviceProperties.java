package eu.europa.ec.simpl.usersroles.configurations;

import java.net.URI;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "microservice")
public record MicroserviceProperties(AuthenticationProvider authenticationProvider) {

    public record AuthenticationProvider(URI url) {}
}
