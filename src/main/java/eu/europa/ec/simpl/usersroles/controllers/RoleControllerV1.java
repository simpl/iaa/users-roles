package eu.europa.ec.simpl.usersroles.controllers;

import eu.europa.ec.simpl.api.usersroles.v1.exchanges.RolesApi;
import eu.europa.ec.simpl.api.usersroles.v1.model.KeycloakRoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.PageResponseRoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.RoleDTO;
import eu.europa.ec.simpl.api.usersroles.v1.model.SearchRolesFilterParameterDTO;
import eu.europa.ec.simpl.usersroles.mappers.RoleMapperV1;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class RoleControllerV1 implements RolesApi {

    private final RoleController controller;
    private final RoleMapperV1 mapper;

    public RoleControllerV1(RoleController controller, RoleMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public RoleDTO create(KeycloakRoleDTO keycloakRoleDTO) {
        return mapper.toV1(controller.create(mapper.toV0(keycloakRoleDTO)));
    }

    @Override
    public void deleteAttributeFromRole(UUID roleId, String attributeCode) {
        controller.deleteAttributeFromRole(roleId, attributeCode);
    }

    @Override
    public void deleteAttributeFromRoleDeprecated(UUID roleId, String attributeCode) {
        deleteAttributeFromRole(roleId, attributeCode);
    }

    @Override
    public void deleteCredential(UUID roleId) {
        controller.delete(roleId);
    }

    @Override
    public void duplicateIdentityAttributeToAnOtherRole(UUID id, String body) {
        controller.duplicateIdentityAttributeToAnOtherRole(id, body);
    }

    @Override
    public void duplicateIdentityAttributeToAnOtherRoleDeprecated(UUID roleId, String body) {
        duplicateIdentityAttributeToAnOtherRole(roleId, body);
    }

    @Override
    public RoleDTO findById(UUID id) {
        return mapper.toV1(controller.findById(id));
    }

    @Override
    public List<String> getIdentityAttributesFromRoleList(List<String> requestBody) {
        return controller.getIdentityAttributesFromRoleList(requestBody);
    }

    @Override
    public void importRoles(List<KeycloakRoleDTO> keycloakRoleDTO) {
        controller.importRoles(mapper.toV0(keycloakRoleDTO));
    }

    @Override
    public void replaceIdentityAttributes(UUID id, List<String> requestBody) {
        controller.replaceIdentityAttributes(id, requestBody);
    }

    @Override
    public PageResponseRoleDTO searchRoles(
            Integer page, Integer size, List<String> sort, SearchRolesFilterParameterDTO filter) {
        return mapper.toV1(controller.search(
                mapper.toV0(filter), PageRequest.of(page, size, Sort.by(sort.toArray(String[]::new)))));
    }

    @Override
    public RoleDTO update(UUID roleId, RoleDTO roleDTO) {
        return mapper.toV1(controller.update(mapper.toV0(roleDTO)));
    }
}
