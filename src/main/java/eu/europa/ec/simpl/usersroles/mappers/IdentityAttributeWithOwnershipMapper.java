package eu.europa.ec.simpl.usersroles.mappers;

import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeWithOwnershipDTO;
import eu.europa.ec.simpl.usersroles.entities.IdentityAttributeWithOwnership;
import java.util.List;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

// TODO decide to ignore (unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@Mapper
@AnnotateWith(Generated.class)
public interface IdentityAttributeWithOwnershipMapper {

    IdentityAttributeWithOwnership toEntity(IdentityAttributeDTO dto, Boolean assignedToParticipant);

    @Mapping(target = ".", source = "identityAttribute")
    IdentityAttributeWithOwnership toEntity(IdentityAttributeWithOwnershipDTO dto);

    @Mapping(target = "identityAttribute", source = ".")
    IdentityAttributeWithOwnershipDTO toLightDtoWithOwnership(IdentityAttributeWithOwnership entity);

    @Mapping(target = "participantTypes", ignore = true)
    IdentityAttributeDTO toLightDto(IdentityAttributeWithOwnership entity);

    void updateIdentityAttribute(@MappingTarget IdentityAttributeWithOwnership entity, IdentityAttributeDTO ida);

    @Mapping(target = "identityAttribute.id", source = "id")
    @Mapping(target = "identityAttribute.code", source = "code")
    @Mapping(target = "identityAttribute.name", source = "name")
    @Mapping(target = "identityAttribute.description", source = "description")
    @Mapping(target = "identityAttribute.assignableToRoles", source = "assignableToRoles")
    @Mapping(target = "identityAttribute.enabled", source = "enabled")
    @Mapping(target = "identityAttribute.creationTimestamp", source = "creationTimestamp")
    @Mapping(target = "identityAttribute.updateTimestamp", source = "updateTimestamp")
    @Mapping(target = "identityAttribute.participantTypes", source = "participantTypes")
    @Mapping(target = "identityAttribute.used", source = "used")
    IdentityAttributeWithOwnershipDTO toDto(
            eu.europa.ec.simpl.api.authenticationprovider.v1.model.IdentityAttributeWithOwnershipDTO content);

    default List<IdentityAttributeWithOwnershipDTO> toListDto(
            List<eu.europa.ec.simpl.api.authenticationprovider.v1.model.IdentityAttributeWithOwnershipDTO> content) {
        if (content != null) {
            return content.stream().map(this::toDto).toList();
        } else {
            return null;
        }
    }
}
