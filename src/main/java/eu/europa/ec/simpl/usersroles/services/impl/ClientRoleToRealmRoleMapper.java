package eu.europa.ec.simpl.usersroles.services.impl;

import eu.europa.ec.simpl.usersroles.configurations.keycloak.KeycloakProperties;
import eu.europa.ec.simpl.usersroles.services.KeycloakService;
import eu.europa.ec.simpl.usersroles.services.KeycloakUserService;
import jakarta.annotation.PostConstruct;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@ConditionalOnProperty(value = "keycloak.client-to-realm-role-migration.enabled", havingValue = "true")
public class ClientRoleToRealmRoleMapper {

    private final KeycloakService keycloakService;
    private final KeycloakUserService keycloakUserService;
    private final KeycloakProperties keycloakProperties;

    private RealmResource realmResource;
    private ClientsResource clientsResource;

    public ClientRoleToRealmRoleMapper(
            KeycloakUserService keycloakUserService,
            KeycloakService keycloakService,
            KeycloakProperties keycloakProperties) {
        this.keycloakUserService = keycloakUserService;
        this.keycloakService = keycloakService;
        this.keycloakProperties = keycloakProperties;
    }

    @PostConstruct
    public void copy() {
        this.realmResource = keycloakService.getAppRealm();
        this.clientsResource = realmResource.clients();
        var usersResource = realmResource.users();

        var clients =
                getClientsByIds(keycloakProperties.clientToRealmRoleMigration().clientIds());
        var clientRoles = getClientRoles(clients, clientsResource);
        log.info("Found {} client roles in the Keycloak realm", clientRoles.size());

        importRealmRolesFromClientRoles(clientRoles, realmResource);

        usersResource.list().forEach(user -> assignRealmRolesIfPresents(user, getClientRolesForUser(user, clients)));
        if (!clientRoles.isEmpty()) {
            log.info("Realm roles assigned to users successfully");
        }
    }

    private List<ClientRepresentation> getClientsByIds(List<String> clientIds) {
        log.info("getting clients by ids: {}", clientIds);
        return clientIds.stream()
                .flatMap(clientId -> clientsResource.findByClientId(clientId).stream())
                .toList();
    }

    private void importRealmRolesFromClientRoles(Set<RoleRepresentation> clientRoles, RealmResource realmResource) {
        clientRoles.forEach(role -> {
            try {
                if (!keycloakUserService.isRolePresent(role.getName())) {
                    var newRealmRole = new RoleRepresentation();
                    newRealmRole.setName(role.getName());
                    newRealmRole.setDescription(role.getDescription());

                    realmResource.roles().create(newRealmRole);
                    log.info("Created new realm role: {}", role.getName());
                } else {
                    log.info("Realm role already exists: {}", role.getName());
                }
            } catch (Exception e) {
                log.error("Error creating realm role for client role: {}", role.getName(), e);
            }
        });
    }

    private static Set<RoleRepresentation> getClientRoles(
            List<ClientRepresentation> clients, ClientsResource clientsResource) {
        return clients.stream()
                .flatMap(client -> clientsResource.get(client.getId()).roles().list().stream())
                .filter(role -> !role.getDescription().startsWith("$"))
                .collect(Collectors.toSet());
    }

    private Set<RoleRepresentation> getClientRolesForUser(UserRepresentation user, List<ClientRepresentation> clients) {
        return clients.stream()
                .flatMap(client -> {
                    String clientId = client.getId();
                    return realmResource.users().get(user.getId()).roles().clientLevel(clientId).listAll().stream();
                })
                .collect(Collectors.toSet());
    }

    private void assignRealmRolesIfPresents(UserRepresentation user, Set<RoleRepresentation> clientRoles) {
        if (clientRoles.isEmpty()) {
            log.info("No role assignable for user: {} {}", user.getFirstName(), user.getLastName());
        } else {
            assignRealmRolesToUser(user, clientRoles);
        }
    }

    private void assignRealmRolesToUser(UserRepresentation user, Set<RoleRepresentation> clientRoles) {
        var userResource = realmResource.users().get(user.getId());

        var rolesAlreadyAssigned = userResource.roles().realmLevel().listAll().stream()
                .map(RoleRepresentation::getName)
                .collect(Collectors.toSet());

        var rolesToAssign = clientRoles.stream()
                .map(RoleRepresentation::getName)
                .filter(roleName -> !rolesAlreadyAssigned.contains(roleName))
                .toList();

        if (!rolesToAssign.isEmpty()) {
            var rolesToAdd = rolesToAssign.stream()
                    .map(roleName -> realmResource.roles().get(roleName).toRepresentation())
                    .toList();
            userResource.roles().realmLevel().add(rolesToAdd);
            log.info(
                    "Assigned {} new realm roles to user: {} {}",
                    rolesToAssign.size(),
                    user.getFirstName(),
                    user.getLastName());
        } else {
            log.info("No new realm roles to assign for user: {} {}}", user.getFirstName(), user.getLastName());
        }
    }
}
