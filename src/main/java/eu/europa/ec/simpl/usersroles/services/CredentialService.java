package eu.europa.ec.simpl.usersroles.services;

public interface CredentialService {

    byte[] getCredential();
}
