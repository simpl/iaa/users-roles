# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2024-11-28 (todo: replace with actual release date)

### Added
- Added component to copy client roles into realm roles.
- Added Role-IDAs mapping initializer.

### Changed
- Updated logical data model.
- Upgraded `simpl-http-client` version to 0.7.3 .
- Replaced `role_id` with `roleName` (_VARCHAR_) in `identity_attribute_roles` table.
- Role service does not rely on local database table anymore.
- Replaced deleteRoleByName with `deleteRoleById` on `RoleController`.

### Removed
- Deleted `role` table from DB

## [0.7.3] - 2024-11-25

### Changed
- Update http client version

## [0.7.2] - 2024-11-18

### Changed
- Update http client version

## [0.7.1] - 2024-11-13

### Changed
- Update http client version

## [0.7.0] - 2024-11-11

### Added
- Credential ID endpoint 
- ParticipantType to logical data model
- RolesDTo with assigned Ida
- getting private key from exchange
- Added logout endpoint

### Changed
- /session/credential is now POST instead of GET
- credential has now pem format
- adapting logic to use credential-id instead of participant-id
- moved logout to SessionController

### Removed

- Remove participant type enum